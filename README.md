# gql2hs

## Usage

    λ gql2hs << EOF
    enum Bool {
      FALSE
      TRUE
    }

    scalar True
    scalar False

    union Boolean = True | False

    type Record {
      a: A
      b: B!
      c: [C!]!
      d(e: E): D
    }
    EOF
    data Bool = FALSE
              | TRUE
    type True = ()
    type False = ()
    data Boolean = Boolean__0 True
                 | Boolean__1 False
    data Record = Record{a :: A, b :: B, c :: C, d :: E -> D}
