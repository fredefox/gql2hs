{-# OPTIONS_GHC -Wall #-}
module Main where

import Control.Category ((>>>))
import Control.Monad.Fail (fail)
import Data.Attoparsec.Text (Parser)
import Data.Foldable (traverse_)
import Data.Text (Text)
import Language.Haskell.Exts.Pretty
import Prelude hiding (fail)
import qualified Data.Attoparsec.Text as Parser
import qualified Data.Text.IO as Text
import qualified GraphQL.Internal.Syntax.AST as GraphQL
import qualified GraphQL.Internal.Syntax.Parser as GraphQL.Parser
import qualified Language.GraphQL.Reflection as GraphQL

main :: IO ()
main = run

run :: IO ()
run = parseContents >>= (GraphQL.schemaDocument >>> traverse_ (prettyPrint >>> putStrLn))

parseContents :: IO GraphQL.SchemaDocument
parseContents = do
  txt <- Text.getContents
  parse GraphQL.Parser.schemaDocument txt

parse :: Parser a -> Text -> IO a
parse p = Parser.parseOnly p >>> either fail pure
